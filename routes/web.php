<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::get('inicio', 'HomeController@index');
Route::get('vehiculos', 'VehiculoController@vehiculos');
Route::get('propietarios', 'PropietarioController@propietarios');
Route::get('parqueo', 'ParqueoController@parqueo');
Route::get('tarifas', 'TarifaController@tarifas');



/* PRIPETARIOS API */
Route::post('regNewPropietario', 'PropietarioController@regNewPropietario');
Route::get('consultaPropietarioID', 'PropietarioController@consultaPropietarioID');


/* Vehiculos API */
Route::post('regNewVehiculo', 'VehiculoController@regNewVehiculo');
Route::get('infoVehiculoPlaca', 'VehiculoController@infoVehiculoPlaca');


/* Tarifas API */
Route::post('regNewTarifa', 'TarifaController@regNewTarifa');
Route::post('regPromocion', 'TarifaController@regPromocion');

/* Parqueo API */
Route::get('posicionesDisponibles', 'ParqueoController@posicionesDisponibles');
Route::post('regNewParqueo', 'ParqueoController@regNewParqueo');
Route::get('retirarVehiculo', 'ParqueoController@retirarVehiculo');








