
  <div class="modal fade" id="modal_regVehiculo" tabindex="-1" role="dialog" aria-labelledby="largeModalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="text-center">NUEVO VEHICULO</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body">
                <form id="formRegVehiculo">
                    @csrf
                <div class="row">
                    <div class="col-md-4">
                        <label>Tipo Vehiculo</label>
                        <select class="form-control" name="tipoVehiculo" id="tipoVehiculo">
                            <option value="0">--Seleccione--</option>
                            <option value="Automovil">Automovil</option>
                            <option value="Moto">Moto</option>
                            <option value="Bicicleta">Bicicleta</option>
                        </select>
                    </div>
                    <div class="col-md-4">
                        <label>Marca</label>
                        <input class="form-control" type="text" name="marcaVehiculo" id="marcaVehiculo">
                    </div>
                    <div class="col-md-4">
                        <label>Modelo</label>
                        <input class="form-control" type="text" name="modeloVehiculo" id="modeloVehiculo">
                    </div>
                    </div>
                    <br>
                    <div class="row">
                    <div class="col-md-4">
                        <label>Placa o Serial</label>
                        <input class="form-control" type="text" name="placaVehiculo" id="placaVehiculo">
                    </div>
                    <div class="col-md-4">
                        <label>ID Propietario Vehiculo</label>
                        <input class="form-control" type="number" name="numIDPropietario" id="numIDPropietario">
                        <span id="textBusquedaPropietario"></span>
                    </div>
                    <div class="col-md-4">
                        <label>Propietario Vehiculo</label>
                        <input class="form-control" type="text" id="namePropietario" disabled>
                    </div>
                </div>
            </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
                <button type="button" class="btn btn-primary" id="btnGuardarVehiculo" disabled>Guardar</button>
            </div>
        </div>
    </div>
</div>