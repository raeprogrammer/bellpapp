@extends('layouts/index')
@section('content')
<div class="main-content">
    <div class="section__content section__content--p30">
        <div class="container-fluid">
    <div class="row">
        <div class="col-md-12">
            <h3 class="text-center">VEHICULOS</h3>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
   
            <button type="button" class="btn btn-outline-primary" onclick="regVehiculo()">REGISTRAR VEHICULO</button>
        
        </div>
    </div>
    <hr>
    <div class="row">
        <div class="col-md-12">
            <!-- DATA TABLE-->
            <div class="table-responsive m-b-40">
                <table class="table table-borderless table-data3">
                    <thead>
                        <tr>
                            <th>N°</th>
                            <th>Tipo</th>
                            <th>Marca</th>
                            <th>Modelo</th>
                            <th>Placa</th>
                            <th>Propietario</th>
                            <th>ID Propietario</th>

                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($vehiculos as $vehiculo)
                        <tr>
                            <td>{{$loop->iteration}}</td>
                            <td>{{$vehiculo->tipo}}</td>
                            <td>{{$vehiculo->marca}}</td>
                            <td>{{$vehiculo->modelo}}</td>
                            <td>{{$vehiculo->placa}}</td>
                            <td>{{$vehiculo->nombre}}</td>
                            <td>{{$vehiculo->numIdentificacion}}</td>
                        </tr>
                        @endforeach
        
                    </tbody>
                </table>
            </div>
            <!-- END DATA TABLE-->
        </div>
    </div>
</div>
</div>
</div>
@include('layouts/vehiculos/modals/modal_regVehiculo')
@endsection
@section('extra-script')
<script src="js/vehiculos/vehiculos.js"></script>
@endsection