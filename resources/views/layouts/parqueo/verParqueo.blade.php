@extends('layouts/index')
@section('content')
<div class="main-content">
    <div class="section__content section__content--p30">
        <div class="container-fluid">
    <div class="row">
        <div class="col-md-12">
            <h3 class="text-center">PARQUEO</h3>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <button type="button" class="btn btn-outline-primary" onclick="newParqueo()">NUEVO INGRESO</button>
        </div>
    </div>
    <hr>

    <div class="row">
        <div class="col-md-12">
            <div class="card border border-secondary">
                <div class="card-header">
                    <strong class="card-title">AUTOMOVILES</strong>
                </div>
                <div class="card-body">
                    <div class="row">
                        @foreach ($posicionesAutos as $posicionAuto)
                        @if ($posicionAuto->estado=="Ocupado")
                        <div class="col-md-3">
                            
                            <div class="card bg-danger">
                                <a href="#" onclick="liberarPosicion({{$posicionAuto->idPosicion}}, '{{$posicionAuto->placaVehiculo}}')">
                                <div class="card-body">
                                    <blockquote class="blockquote mb-0 text-light">
                                        <p class="text-light" style="font-size: 14px">
                                            <span>Posicion: <strong>{{$posicionAuto->numPosicion}}</strong></span> <br>
                                            <span>Placa: {{$posicionAuto->placaVehiculo}}</span> <br>
                                            <span>Marca: {{$posicionAuto->marcaVeh}}</span> <br>
                                            <span>Modelo: {{$posicionAuto->modeloVeh}}</span>


                                        </p>
                                      
                                    </blockquote>
                                </div>
                            </a>
                            </div>
                        
                        </div>  
                        @else
                        <div class="col-md-3">
                            <div class="card bg-success">
                                <div class="card-body">
                                    <blockquote class="blockquote mb-0 text-light">
                                        <p class="text-light" style="font-size: 14px">
                                            <span>Posicion: <strong>{{$posicionAuto->numPosicion}}</strong></span> <br>
                                            <span>Placa: </span> <br>
                                            <span>Marca: </span> <br>
                                            <span>Modelo: </span>
                                        </p>
                                    </blockquote>
                                </div>
                            </div>
                        </div> 
                        @endif
                
                        @endforeach
       
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="card border border-secondary">
                <div class="card-header">
                    <strong class="card-title">MOTOS</strong>
                </div>
                <div class="card-body">
                    <div class="row">
                        @foreach ($posicionesMoto as $posicionMoto)
                        @if ($posicionMoto->estado=="Ocupado")
                        <div class="col-md-3">
                            
                            <div class="card bg-danger">
                                <a href="#" onclick="liberarPosicion({{$posicionMoto->idPosicion}}, '{{$posicionMoto->placaVehiculo}}')">
                                <div class="card-body">
                                    <blockquote class="blockquote mb-0 text-light">
                                        <p class="text-light" style="font-size: 14px">
                                            <span>Posicion: <strong>{{$posicionMoto->numPosicion}}</strong></span> <br>
                                            <span>Placa: {{$posicionMoto->placaVehiculo}}</span> <br>
                                            <span>Marca: {{$posicionMoto->marcaVeh}}</span> <br>
                                            <span>Modelo: {{$posicionMoto->modeloVeh}}</span>


                                        </p>
                                      
                                    </blockquote>
                                </div>
                            </a>
                            </div>
                        
                        </div>  
                        @else
                        <div class="col-md-3">
                            <div class="card bg-success">
                                <div class="card-body">
                                    <blockquote class="blockquote mb-0 text-light">
                                        <p class="text-light" style="font-size: 14px">
                                            <span>Posicion: <strong>{{$posicionMoto->numPosicion}}</strong></span> <br>
                                            <span>Placa: </span> <br>
                                            <span>Marca: </span> <br>
                                            <span>Modelo: </span>
                                        </p>
                                    </blockquote>
                                </div>
                            </div>
                        </div> 
                        @endif
                
                        @endforeach
       
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="card border border-secondary">
                <div class="card-header">
                    <strong class="card-title">BICICLETAS</strong>
                </div>
                <div class="card-body">
                    <div class="row">
                        @foreach ($posicionesBicicleta as $posicionBici)
                        @if ($posicionBici->estado=="Ocupado")
                        <div class="col-md-3">
                            
                            <div class="card bg-danger">
                                <a href="#" onclick="liberarPosicion({{$posicionBici->idPosicion}}, '{{$posicionBici->placaVehiculo}}')">
                                <div class="card-body">
                                    <blockquote class="blockquote mb-0 text-light">
                                        <p class="text-light" style="font-size: 14px">
                                            <span>Posicion: <strong>{{$posicionBici->numPosicion}}</strong></span> <br>
                                            <span>Placa: {{$posicionBici->placaVehiculo}}</span> <br>
                                            <span>Marca: {{$posicionBici->marcaVeh}}</span> <br>
                                            <span>Modelo: {{$posicionBici->modeloVeh}}</span>


                                        </p>
                                      
                                    </blockquote>
                                </div>
                            </a>
                            </div>
                        
                        </div>  
                        @else
                        <div class="col-md-3">
                            <div class="card bg-success">
                                <div class="card-body">
                                    <blockquote class="blockquote mb-0 text-light">
                                        <p class="text-light" style="font-size: 14px">
                                            <span>Posicion: <strong>{{$posicionBici->numPosicion}}</strong></span> <br>
                                            <span>Placa: </span> <br>
                                            <span>Marca: </span> <br>
                                            <span>Modelo: </span>
                                        </p>
                                    </blockquote>
                                </div>
                            </div>
                        </div> 
                        @endif
                
                        @endforeach
       
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</div>
</div>
@include('layouts/parqueo/modals/modal_newParqueo')
@endsection
@section('extra-script')
<script src="js/parqueo/parqueo.js"></script>
@endsection