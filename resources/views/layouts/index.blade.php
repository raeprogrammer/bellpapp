<!DOCTYPE html>
<html lang="es">

<head>
    <!-- Required meta tags-->
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="au theme template">
    <meta name="author" content="Hau Nguyen">
    <meta name="keywords" content="au theme template">

    <!-- Title Page-->
    <title>Dashboard</title>

    <!-- Fontfaces CSS-->
    <link href="css/font-face.css" rel="stylesheet" media="all">
    <link href="cooladmin/vendor/font-awesome-4.7/css/font-awesome.min.css" rel="stylesheet" media="all">
    <link href="cooladmin/vendor/font-awesome-5/css/fontawesome-all.min.css" rel="stylesheet" media="all">
    <link href="cooladmin/vendor/mdi-font/css/material-design-iconic-font.min.css" rel="stylesheet" media="all">

    <!-- Bootstrap CSS-->
    <link href="cooladmin/vendor/bootstrap-4.1/bootstrap.min.css" rel="stylesheet" media="all">

    <!-- Vendor CSS-->
    <link href="cooladmin/vendor/animsition/animsition.min.css" rel="stylesheet" media="all"> 
    <link href="cooladmin/vendor/bootstrap-progressbar/bootstrap-progressbar-3.3.4.min.css" rel="stylesheet" media="all">
    <link href="cooladmin/vendor/wow/animate.css" rel="stylesheet" media="all">
    <link href="cooladmin/vendor/css-hamburgers/hamburgers.min.css" rel="stylesheet" media="all">
    <link href="cooladmin/vendor/slick/slick.css" rel="stylesheet" media="all">
    <link href="cooladmin/vendor/select2/select2.min.css" rel="stylesheet" media="all">
    <link href="cooladmin/vendor/perfect-scrollbar/perfect-scrollbar.css" rel="stylesheet" media="all">

    <!-- Main CSS-->
    <link href="cooladmin/css/theme.css" rel="stylesheet" media="all">
    <link rel="stylesheet" href="cooladmin/vendor/sweetalert/sweetalert.css">

    <link href="css/miestilos.css" rel="stylesheet" media="all">
</head>

<body class="animsition">
    <div class="page-wrapper">
        <!-- HEADER MOBILE-->
        @include('layouts/menu/menuNavegaMovil')
        <!-- END HEADER MOBILE-->
        @include('layouts/menu/menuNavegaWeb')
        <!-- END MENU SIDEBAR-->

        <!-- PAGE CONTAINER-->
        <div class="page-container">
            <!-- HEADER DESKTOP-->
            @include('layouts/menu/header')
            <!-- HEADER DESKTOP-->

            <!-- MAIN CONTENT-->
            <section class="content" id="seccionPrincipal">
            
                        
                            @yield('content')
                        
 
        
        
            </section>
            <!-- END MAIN CONTENT-->
            <!-- END PAGE CONTAINER-->
        </div>

    </div>

    <!-- Jquery JS-->
    <script src="cooladmin/vendor/jquery-3.2.1.min.js"></script>
    <!-- Bootstrap JS-->
    <script src="cooladmin/vendor/bootstrap-4.1/popper.min.js"></script>
    <script src="cooladmin/vendor/bootstrap-4.1/bootstrap.min.js"></script>
    <!-- Vendor JS       -->
    <script src="cooladmin/vendor/slick/slick.min.js">
    </script>
    <script src="cooladmin/vendor/wow/wow.min.js"></script>
    <script src="cooladmin/vendor/animsition/animsition.min.js"></script>
    <script src="cooladmin/vendor/bootstrap-progressbar/bootstrap-progressbar.min.js">
    </script>
    <script src="cooladmin/vendor/counter-up/jquery.waypoints.min.js"></script>
    <script src="cooladmin/vendor/counter-up/jquery.counterup.min.js">
    </script>
    <script src="cooladmin/vendor/circle-progress/circle-progress.min.js"></script>
    <script src="cooladmin/vendor/perfect-scrollbar/perfect-scrollbar.js"></script>
    <script src="cooladmin/vendor/chartjs/Chart.bundle.min.js"></script>
    <script src="cooladmin/vendor/select2/select2.min.js"></script>


    <!-- Main JS-->
    <script src="cooladmin/js/main.js"></script>
    <script src="cooladmin/vendor/sweetalert/sweetalert.min.js"></script>
    @yield('extra-script')
</body>

</html>
<!-- end document-->
