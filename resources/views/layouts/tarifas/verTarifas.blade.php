@extends('layouts/index')
@section('content')
<div class="main-content">
    <div class="section__content section__content--p30">
        <div class="container-fluid">
    <div class="row">
        <div class="col-md-12">
            <h3 class="text-center">TARIFAS</h3>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <button type="button" class="btn btn-outline-primary" onclick="newTarifa()">NUEVA TARIFA</button>
            <button type="button" class="btn btn-outline-primary" onclick="promociones()">PROMOCION</button>
        </div>
    </div>
    <hr>
    <div class="row">
        <div class="col-md-12">
            <!-- DATA TABLE-->
            <div class="table-responsive m-b-40">
                <table class="table table-borderless table-data3">
                    <thead>
                        <tr>
                            <th>N°</th>
                            <th>Tipo Vehiculo</th>
                            <th>Valor Minuto</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($tarifas as $tarifa)
                            <tr>
                                <td>{{$loop->iteration}}</td>
                                <td>{{$tarifa->tipoVehiculo}}</td>
                                <td>{{$tarifa->valorMinuto}}</td>
                            </tr>
                        @endforeach
        
                    </tbody>
                </table>
            </div>
            <!-- END DATA TABLE-->
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <!-- DATA TABLE-->
            <div class="table-responsive m-b-40">
                <table class="table table-borderless table-data3">
                    <thead>
                        <tr>
                            <th>Nombre Promocion</th>
                            <th>Descuento</th>
                            <th>Minutos</th>

                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($promocion as $promo)
                            <tr>
                                <td>{{$promo->nombre_promocion}}</td>
                                <td>{{$promo->descuento}} %</td>
                                <td>{{$promo->minutos}}</td>
                            </tr>
                        @endforeach
        
                    </tbody>
                </table>
            </div>
            <!-- END DATA TABLE-->
        </div>
    </div>
</div>
</div>
</div>
@include('layouts/tarifas/modals/modal_newTarifa')
@include('layouts/tarifas/modals/modal_promociones')

@endsection
@section('extra-script')
<script src="js/tarifas/tarifas.js"></script>
<script src="js/tarifas/promociones.js"></script>

@endsection