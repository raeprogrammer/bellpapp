
  <div class="modal fade" id="modal_regTarifa" tabindex="-1" role="dialog" aria-labelledby="largeModalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="text-center">NUEVO TARIFA</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body">
                <form id="formRegTarifa">
                    @csrf
                <div class="row">
                    <div class="col-md-6">
                        <label>Tipo Vehiculo</label>
                        <select class="form-control" name="tipoVehiculo" id="tipoVehiculo">
                            <option value="0">--Seleccione--</option>
                            <option value="Automovil">Automovil</option>
                            <option value="Moto">Moto</option>
                            <option value="Bicicleta">Bicicleta</option>
                        </select>
                    </div>
                    <div class="col-md-6">
                        <label>Valor Minuto</label>
                        <input class="form-control" type="number" name="valorMinuto" id="valorMinuto">
                    </div>
                    </div>
                </form>

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
                <button type="button" class="btn btn-primary" id="btnGuardarTarifa">Guardar</button>
            </div>
        </div>
    </div>
</div>