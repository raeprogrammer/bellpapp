
  <div class="modal fade" id="modal_promociones" tabindex="-1" role="dialog" aria-labelledby="largeModalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="text-center">PROMOCIONES</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body">
                <form id="formRegPromocion">
                    @csrf
                <div class="row">
                    <div class="col-md-4">
                        <label>Nombre de Promocion</label>
                        <input class="form-control" type="text" name="nombrePromocion" id="nombrePromocion">
                    </div>
                    <div class="col-md-4">
                        <label>Descuento (%)</label>
                        <input class="form-control" type="number" name="descuento" id="descuento">
                    </div>
                    <div class="col-md-4">
                        <label>Minutos</label>
                        <input class="form-control" type="number" name="minutosPromocion" id="minutosPromocion">
                    </div>
                    </div>
                </form>

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
                <button type="button" class="btn btn-primary" id="btnGuardarPromocion">Guardar</button>
            </div>
        </div>
    </div>
</div>