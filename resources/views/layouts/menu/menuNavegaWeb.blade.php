        <!-- MENU SIDEBAR-->
        <aside class="menu-sidebar d-none d-lg-block">
            <div class="logo">
                <a href="#">
                    <img src="images/logo_bellpi.png" alt="Bellpi App"/>
                </a>
            </div>
            <div class="menu-sidebar__content js-scrollbar1">
                <nav class="navbar-sidebar">
                    <ul class="list-unstyled navbar__list">
                        <li class="has-sub {{Request :: is('inicio')? 'active':''}}">
                            <a href="inicio">
                                <i class="fas fa-home"></i>Inicio</a>
                        </li>
                        <li class="{{Request :: is('propietarios')? 'active':''}}">
                            <a href="propietarios">
                            <i class="fas fa-chart-bar"></i>Propietarios</a>
                        </li>
                        <li class="{{Request :: is('vehiculos')? 'active':''}}">
                            <a href="vehiculos">
                            <i class="fas fa-chart-bar"></i>Vehiculos</a>
                        </li>
                     
                        <li class="{{Request :: is('tarifas')? 'active':''}}">
                            <a href="tarifas">
                            <i class="fas fa-chart-bar"></i>Tarifas</a>
                        </li>
                        <li class="{{Request :: is('parqueo')? 'active':''}}">
                            <a href="parqueo">
                                <i class="fas fa-table"></i>Parqueo</a>
                        </li>
                  
                   
                   
               {{--          <li class="has-sub">
                            <a class="js-arrow" href="#">
                                <i class="fas fa-copy"></i>Pages</a>
                            <ul class="list-unstyled navbar__sub-list js-sub-list">
                                <li>
                                    <a href="login.html">Login</a>
                                </li>
                                <li>
                                    <a href="register.html">Register</a>
                                </li>
                                <li>
                                    <a href="forget-pass.html">Forget Password</a>
                                </li>
                            </ul>
                        </li> --}}
           
                    </ul>
                </nav>
            </div>
        </aside>