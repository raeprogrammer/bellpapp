
  <div class="modal fade" id="modal_regPropietario" tabindex="-1" role="dialog" aria-labelledby="largeModalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="text-center">NUEVO PROPIETARIO</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body">
                <form id="formRegPropietario">
                    @csrf
                <div class="row">
                    <div class="col-md-6">
                        <label>N° Identificacion</label>
                        <input class="form-control" type="number" name="numIdentificacion" id="numIdentificacion">
                    </div>
                    <div class="col-md-6">
                        <label>Nombre Completo</label>
                        <input class="form-control" type="text" name="nombrePropietario" id="nombrePropietario">
                    </div>
                    </div>
                </form>

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
                <button type="button" class="btn btn-primary" id="btnGuardarPropietario">Guardar</button>
            </div>
        </div>
    </div>
</div>