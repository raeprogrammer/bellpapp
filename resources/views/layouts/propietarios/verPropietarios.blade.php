@extends('layouts/index')
@section('content')
<div class="main-content">
    <div class="section__content section__content--p30">
        <div class="container-fluid">
    <div class="row">
        <div class="col-md-12">
            <h3 class="text-center">PROPIETARIOS</h3>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
   
            <button type="button" class="btn btn-outline-primary" onclick="regPropietario()">REGISTRAR PROPIETARIO</button>
        
        </div>
    </div>
    <hr>
    <div class="row">
        <div class="col-md-12">
            <!-- DATA TABLE-->
            <div class="table-responsive m-b-40">
                <table class="table table-borderless table-data3">
                    <thead>
                        <tr>
                            <th>N°</th>
                            <th>Nombre</th>
                            <th>Identificación</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($propietarios as $propietario)
                        <tr>
                            <td>{{$loop->iteration}}</td>
                            <td>{{$propietario->nombre}}</td>
                            <td>{{$propietario->numIdentificacion}}</td>
                            
                        </tr>
                        @endforeach
     
                    </tbody>
                </table>
            </div>
            <!-- END DATA TABLE-->
        </div>
    </div>
</div>
</div>
</div>
@include('layouts/propietarios/modals/modal_regPropietario')
@endsection
@section('extra-script')
<script src="js/propietarios/propietarios.js"></script>
@endsection