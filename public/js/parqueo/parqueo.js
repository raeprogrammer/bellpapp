
function newParqueo(){
    $("#modal_newParqueo").modal('show');
}

$("#tipoVehiculo").change(function(){
    
    var tipoVeh=$("#tipoVehiculo").val();
    posiciones_disponibles(tipoVeh);
});

function posiciones_disponibles(tipoVehiculo){
    $("#posicionVehiculo").html('<option value="0">--Seleccione--</option>');
    $.ajax({
        type: "GET",
        url: "posicionesDisponibles",
        data: "tipoVehiculo="+tipoVehiculo,
        success: function (resp) {
            console.log(resp);
            $.each(resp.data, function (index, datos) { 
                console.log(datos);
                $("#posicionVehiculo").append('<option value="'+datos.idPosicion+'">'+datos.numPosicion+'</option>');
            });
           
        }
    });
    
}

$("#placaVeh").blur(function(){
    var placa=$("#placaVeh").val();
    var tipoVeh=$("#tipoVehiculo").val();
    $.ajax({
        type: "GET",
        url: "infoVehiculoPlaca",
        data: "placa="+placa+"&tipoVeh="+tipoVeh,
        beforeSend:function(){
            $("#textBusquedaVehiculo").html("Buscando...");
        },
        success: function (resp) {
            console.log(resp);
            switch (resp.estado_operacion) {
                case "Exitosa":
                    var datos=resp.data;
                    if(datos.length>0){
                        $("#textBusquedaVehiculo").html("");
                        $("#btnRegParqueo").attr('disabled', false);
                    }else{
                        $("#textBusquedaVehiculo").html("<span class='colred'>Vehiculo No Registrado!</span>");
                        $("#btnRegParqueo").attr('disabled', true);
                    }
                break;
            
                default:
                    
                break;
            }
        }
    });
});

$("#btnRegParqueo").click(function(){
    var tipoVeh=$("#tipoVehiculo").val();
    var posicion=$("#posicionVehiculo").val();
    var placa=$("#placaVeh").val();
    var horaInicio=$("#horaInicio").val();

    if(tipoVeh==0 || posicion==0 || placa.length<2 || horaInicio.length<3){
        swal({
            html: true,
            title: "DATOS INCOMPLETOS",
            text: "<p style='color:black'>Los datos estan incompletos, por favor verifique e intentelo nuevamente</p>",
            type: "error"
        });
    }else{
        swal({
            title: "CONFIRMAR ACCIÓN!",
            text: "Se procedera a ingresar el nuevo parqueo, ¿Desea Continuar?",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "CONTINUAR!",
            cancelButtonText: "CANCELAR!",
            closeOnConfirm: false,
            closeOnCancel: true,
            showLoaderOnConfirm: true
        }, function (isConfirm) {
            if(isConfirm){
              var datos=$("#formRegParqueo").serialize();
              regNewParqueo(datos);
            } 
        });  
    }
});

function regNewParqueo(datos){
    $.ajax({
        type: "POST",
        url: "regNewParqueo",
        data: datos,
        success: function (resp) {
            console.log(resp);

            switch (resp.estado_operacion) {
                case "Exitosa":
                    swal({
                        title: "Registro Exitoso!",
                        text: "Se ha registrado la información correctamente",
                        type: "success",
                        showCancelButton: false,
                        showConfirmButton: false
                      });
                      setTimeout(function () {
                        location.reload();
                      }, 1000); 
                break;
            
                case "Fallida":
                    swal({
                        html: true,
                        title: "OPERACIÓN FALLIDA",
                        text: "<p style='color:black'>Se ha presentado un error durante el registro. <br> ERROR:"+resp.data+"</p>",
                        type: "error"
                    });  
                break;
                case "Denegada":
                    swal({
                        html: true,
                        title: "OPERACIÓN DENEGADA",
                        text: "<p style='color:black'>El vehiculo se encuentra activo en una posicion del parqueadero</p>",
                        type: "error"
                    }); 
                break;
                default:
                    swal({
                        html: true,
                        title: "UPS! RESUESTA DESCONOCIDA!",
                        text: "<p style='color:black'>Se ha presentado un error durante la conexión, por favor verifique e intenta nuevamente.</p>",
                        type: "error"
                    });
                break;
            }
        },
        error:function(){
            swal({
                html: true,
                title: "UPS! RESUESTA DESCONOCIDA!",
                text: "<p style='color:black'>Se ha presentado un error durante la conexión, por favor verifique e intenta nuevamente.</p>",
                type: "error"
            });
        }
    });
}

function liberarPosicion(idPosicion, placa){
    swal({
        html: true,
        title: "RETIRAR "+placa,
        text: "Se procedera a retirar el vehiculo del parqueadero. ¿Desea Continuar?",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: "CONTINUAR!",
        cancelButtonText: "CANCELAR!",
        closeOnConfirm: false,
        closeOnCancel: true,
        showLoaderOnConfirm: true
    }, function (isConfirm) {
        if(isConfirm){
         
          retirarVehiculo(idPosicion);
        } 
    }); 
}

function retirarVehiculo(idPosicion){
    $.ajax({
        type: "GET",
        url: "retirarVehiculo",
        data: "idPosicion="+idPosicion,
        success: function (resp) {
            console.log(resp);
            switch (resp.estado_operacion) {
                case "Exitosa":
                    swal({
                        title: "Liberación Exitosa!",
                        text: "Se ha retirado el vehiculo",
                        type: "success",
                        showCancelButton: false,
                        showConfirmButton: false
                      });
                      setTimeout(function () {
                        location.reload();
                      }, 1000); 
                break;
            
                default:
                    swal({
                        html: true,
                        title: "UPS! RESUESTA DESCONOCIDA!",
                        text: "<p style='color:black'>Se ha presentado un error durante la conexión, por favor verifique e intenta nuevamente.</p>",
                        type: "error"
                    });
                break;
            }
        },
        error:function(){
            swal({
                html: true,
                title: "UPS! RESUESTA DESCONOCIDA!",
                text: "<p style='color:black'>Se ha presentado un error durante la conexión, por favor verifique e intenta nuevamente.</p>",
                type: "error"
            });
        }
    });
}