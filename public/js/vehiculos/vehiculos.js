function regVehiculo(){
    $("#modal_regVehiculo").modal('show');
}

$("#numIDPropietario").blur(function(){
    var numID=$("#numIDPropietario").val();
    $.ajax({
        type: "GET",
        url: "consultaPropietarioID",
        data: "numIDPropietario="+numID,
        beforeSend:function(){
            $("#textBusquedaPropietario").html("Buscando...");
        },
        success: function (resp) {
            switch (resp.estado_operacion) {
                case "Exitosa":
                    $("#textBusquedaPropietario").html("");
                    var datos=resp.data;
                    if(datos.length>0){
                        $("#namePropietario").val(datos[0].nombre);
                        $("#btnGuardarVehiculo").attr('disabled', false);
                    }else{
                        $("#namePropietario").val("");
                        $("#btnGuardarVehiculo").attr('disabled', true);
                        $("#textBusquedaPropietario").html("<span class='colred'>Propietario No Registrado!</span>");
                    }
                break;
            
                default:
                break;
            }
          
        }
    });
    
});

$("#btnGuardarVehiculo").click(function(){
    var tipoVeh=$("#tipoVehiculo").val();
    var marcaVeh=$("#marcaVehiculo").val();
    var modeloVeh=$("#modeloVehiculo").val();
    var placaVeh=$("#placaVehiculo").val();
    var idPropietario=$("#numIDPropietario").val();
    if(tipoVeh==0 || marcaVeh.length<3 || modeloVeh.length<3 || placaVeh.length<3 || idPropietario.length<3){
        swal({
            html: true,
            title: "DATOS INCOMPLETOS",
            text: "<p style='color:black'>Los datos estan incompletos, por favor verifique e intentelo nuevamente</p>",
            type: "error"
        });  
    }else{
        swal({
            title: "CONFIRMAR ACCIÓN!",
            text: "Se procedera a registrar al nuevo vehiculo, ¿Desea Continuar?",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "CONTINUAR!",
            cancelButtonText: "CANCELAR!",
            closeOnConfirm: false,
            closeOnCancel: true,
            showLoaderOnConfirm: true
        }, function (isConfirm) {
            if(isConfirm){
              var datos=$("#formRegVehiculo").serialize();
              regNewVehiculo(datos);
            } 
        });     
    }

});

function regNewVehiculo(datos){
    $.ajax({
        type: "POST",
        url: "regNewVehiculo",
        data: datos,
        success: function (resp) {
            console.log(resp);
            switch (resp.estado_operacion) {
                case "Exitosa":
                    swal({
                        title: "Registro Exitoso!",
                        text: "Se ha registrado la información correctamente",
                        type: "success",
                        showCancelButton: false,
                        showConfirmButton: false
                      });
                      setTimeout(function () {
                        location.reload();
                      }, 1000);
                break;

                case "Fallida":
                    swal({
                        html: true,
                        title: "OPERACIÓN FALLIDA",
                        text: "<p style='color:black'>Se ha presentado un error durante el registro. <br> ERROR:"+resp.data+"</p>",
                        type: "error"
                    });  
                break;
            
                default:
                    swal({
                        html: true,
                        title: "UPS! RESUESTA DESCONOCIDA!",
                        text: "<p style='color:black'>Se ha presentado un error durante la conexión, por favor verifique e intenta nuevamente.</p>",
                        type: "error"
                    });
                break;
            }
        },
        error:function(){
            swal({
                html: true,
                title: "UPS! RESUESTA DESCONOCIDA!",
                text: "<p style='color:black'>Se ha presentado un error durante la conexión, por favor verifique e intenta nuevamente.</p>",
                type: "error"
            });
        }
    });
}