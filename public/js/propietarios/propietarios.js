
function regPropietario(){
    $("#modal_regPropietario").modal('show');
}

$("#btnGuardarPropietario").click(function(){
    var numID=$("#numIdentificacion").val();
    var nombrePropietario=$("#nombrePropietario").val();

    if(numID.length<4 || nombrePropietario.length<4){
      swal({
        html: true,
        title: "DATOS INCOMPLETOS",
        text: "<p style='color:black'>Los datos estan incompletos, por favor verifique e intentelo nuevamente</p>",
        type: "error"
    });   
    }else{

      swal({
        title: "CONFIRMAR ACCIÓN!",
        text: "Se procedera a registrar al nuevo propietario, ¿Desea Continuar?",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: "CONTINUAR!",
        cancelButtonText: "CANCELAR!",
        closeOnConfirm: false,
        closeOnCancel: true,
        showLoaderOnConfirm: true
    }, function (isConfirm) {
        if(isConfirm){
          var datos=$("#formRegPropietario").serialize();
          regNewPropietario(datos);
        } 
    });          
    }
  

});

function regNewPropietario(datos){

  $.ajax({
      type: "POST",
      url: "regNewPropietario",
      data: datos,
      success: function (resp) {
          console.log(resp);
          switch (resp.estado_operacion) {
            case "Exitosa":
              swal({
                title: "Registro Exitoso!",
                text: "Se ha registrado la información correctamente",
                type: "success",
                showCancelButton: false,
                showConfirmButton: false
              });
              setTimeout(function () {
                location.reload();
              }, 1000);
            break;
          
            default:
              break;
          }
      }
  });
}