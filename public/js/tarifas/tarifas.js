function newTarifa(){
    $("#modal_regTarifa").modal('show');
}

$("#btnGuardarTarifa").click(function(){
    var tipoVeh=$("#tipoVehiculo").val();
    var tarifa=$("#valorMinuto").val();

    if (tipoVeh==0 || tarifa.length<3) {
        swal({
            html: true,
            title: "DATOS INCOMPLETOS",
            text: "<p style='color:black'>Los datos estan incompletos, por favor verifique e intentelo nuevamente</p>",
            type: "error"
        });   
    }else{
        swal({
            title: "CONFIRMAR ACCIÓN!",
            text: "Se procedera a registrar la nueva tarifa, ¿Desea Continuar?",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "CONTINUAR!",
            cancelButtonText: "CANCELAR!",
            closeOnConfirm: false,
            closeOnCancel: true,
            showLoaderOnConfirm: true
        }, function (isConfirm) {
            if(isConfirm){
              var datos=$("#formRegTarifa").serialize();
              regNewTarifa(datos);
            } 
        });  
    }

});

function regNewTarifa(datos){
    $.ajax({
        type: "POST",
        url: "regNewTarifa",
        data: datos,
        success: function (resp) {
            console.log(resp);
            switch (resp.estado_operacion) {
                case "Exitosa":
                    swal({
                        title: "Registro Exitoso!",
                        text: "Se ha actualizado la tarifa correctamente",
                        type: "success",
                        showCancelButton: false,
                        showConfirmButton: false
                      });
                      setTimeout(function () {
                        location.reload();
                      }, 1000);   
                break;
            
                default:
                    swal({
                        html: true,
                        title: "UPS! RESUESTA DESCONOCIDA!",
                        text: "<p style='color:black'>Se ha presentado un error durante la conexión, por favor verifique e intenta nuevamente.</p>",
                        type: "error"
                    });    
                break;
            }
        }, 
        error:function(){
            swal({
                html: true,
                title: "UPS! RESUESTA DESCONOCIDA!",
                text: "<p style='color:black'>Se ha presentado un error durante la conexión, por favor verifique e intenta nuevamente.</p>",
                type: "error"
            });
        }
    });
}