function promociones(){
    $("#modal_promociones").modal('show');
}

$("#btnGuardarPromocion").click(function(){
    var nombre=$("#nombrePromocion").val();
    var descuento=$("#descuento").val();
    var minutos=$("#minutosPromocion").val();

    if(nombre.length<3 || descuento.length==0 || minutos.length==0){
        swal({
            html: true,
            title: "DATOS INCOMPLETOS",
            text: "<p style='color:black'>Los datos estan incompletos, por favor verifique e intentelo nuevamente</p>",
            type: "error"
        });
    }else{
        swal({
            title: "CONFIRMAR ACCIÓN!",
            text: "Se procedera a registrar la nueva promoción, ¿Desea Continuar?",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "CONTINUAR!",
            cancelButtonText: "CANCELAR!",
            closeOnConfirm: false,
            closeOnCancel: true,
            showLoaderOnConfirm: true
        }, function (isConfirm) {
            if(isConfirm){
              var datos=$("#formRegPromocion").serialize();
              regNewPromocion(datos);
            } 
        });   
    }

});

function regNewPromocion(datos){
    $.ajax({
        type: "POST",
        url: "regPromocion",
        data: datos,
        success: function (resp) {
            console.log(resp);
            switch (resp.estado_operacion) {
                case "Exitosa":
                        swal({
                            title: "Registro Exitoso!",
                            text: "Se ha actualizado la promoción correctamente",
                            type: "success",
                            showCancelButton: false,
                            showConfirmButton: false
                          });
                          setTimeout(function () {
                            location.reload();
                          }, 1000);     
                break;
            
                default:
                    swal({
                        html: true,
                        title: "UPS! RESUESTA DESCONOCIDA!",
                        text: "<p style='color:black'>Se ha presentado un error durante la conexión, por favor verifique e intenta nuevamente.</p>",
                        type: "error"
                    });  
                break;
            }
        }
    });
}