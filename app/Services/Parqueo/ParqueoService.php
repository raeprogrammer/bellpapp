<?php
namespace App\Services\Parqueo;
use App\Traits\ConsumeExternalServices;

class ParqueoService
{
    use ConsumeExternalServices;
    public $baseUri;
    public $keyAccess;

    public function __construct(){
        /* Se obtiene la URL del servicio a consumir */
        $this->baseUri=config('services.parqueo.base_uri');
        $this->keyAccess=config('services.parqueo.keyAccess');
    }

    public function listPosicionesLibres($tipoVeh){
        return $this->performRequest('GET', 'listPosicionesLibres?tipoVeh='.$tipoVeh);
    }

    public function regNewParqueo($request){
        return $this->performRequest('POST', 'regNewParqueo', $request);
    }

    public function listPosiciones(){
        return $this->performRequest('GET', 'listPosiciones');

    }

    public function retirarVehiculo($idPosicion){
        return $this->performRequest('GET', 'retirarVehiculo?idPosicion='.$idPosicion);
    }

}
