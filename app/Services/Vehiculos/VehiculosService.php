<?php
namespace App\Services\Vehiculos;
use App\Traits\ConsumeExternalServices;

class VehiculosService
{
    use ConsumeExternalServices;
    public $baseUri;
    public $keyAccess;

    public function __construct(){
        /* Se obtiene la URL del servicio a consumir */
        $this->baseUri=config('services.vehiculos.base_uri');
        $this->keyAccess=config('services.vehiculos.keyAccess');
    }

    public function regNewPropietario($request){
        return $this->performRequest('POST', 'regNewPropietario', $request);
    }

    public function listPropietarios(){
        return $this->performRequest('GET', 'listPropietarios');
    }

    public function infoPropietarioID($numID){
        return $this->performRequest('GET', 'infoPropietarioID?numID='.$numID);
    }

    public function regNewVehiculo($request){
        return $this->performRequest('POST', 'regNewVehiculo', $request);
    }

    public function listVehiculos(){
        return $this->performRequest('GET', 'listVehiculos');
    }

    public function infoVehiculoPlaca($placa, $tipoVeh){
        return $this->performRequest('GET', 'infoVehiculoPlaca?placa='.$placa.'&tipoVeh='.$tipoVeh);

    }
    
}
?>