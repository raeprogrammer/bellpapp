<?php
namespace App\Services\Tarifas;
use App\Traits\ConsumeExternalServices;

class TarifasService
{
    use ConsumeExternalServices;
    public $baseUri;
    public $keyAccess;

    public function __construct(){
        /* Se obtiene la URL del servicio a consumir */
        $this->baseUri=config('services.tarifas.base_uri');
        $this->keyAccess=config('services.tarifas.keyAccess');
    }

    public function regNewTarifa($request){
        return $this->performRequest('POST', 'regNewTarifa', $request);
    }

    public function listTarifas(){
        return $this->performRequest('GET', 'listTarifas');
    }

    public function regNewPromocion($request){
        return $this->performRequest('POST', 'regNewPromocion', $request);
    }

    public function listPromocion(){
        return $this->performRequest('GET', 'listPromocion');
    }
}