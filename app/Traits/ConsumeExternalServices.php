<?php
namespace App\Traits;

use GuzzleHttp\Client;
/**
 * 
 */
trait ConsumeExternalServices
{
    public function performRequest($method, $requestUrl, $formParams=[], $headers=[]){
        try {
            $client=new Client([
                'base_uri'=>$this->baseUri,
            ]);
            if(isset($this->keyAccess)){
                $headers['Autorization']=$this->keyAccess;
            } 

            $response = $client->request($method, $requestUrl, ['form_params'=>$formParams, 
            'headers'=>$headers]);
        } catch (\Exception $e) {
            return $e;
        }


        return $response->getBody()->getContents();
    }

    public function performRequestFile($method, $requestUrl, $file, $fileName, $idDatoVinculado){
        $client=new Client([
            'base_uri'=>$this->baseUri,
        ]);
        if(isset($this->keyAccess)){
            $headers['Autorization']=$this->keyAccess;
        } 
        $response = $client->request($method, $requestUrl, 
        [
            "headers" => [
                "Autorization" => $this->keyAccess,
                 "enctype" => "multipart/form-data"
            ],
            "multipart" => [
                [
                    "name" => "file",
                    "contents" => file_get_contents($file),
                    "filename" => $fileName
                ],
                [
                    "name" => "DatosVinculados",
                    "contents" => $idDatoVinculado,
                ]
            ]
        ]);
        return $response->getBody()->getContents();
    }

/*     public function performRequestFileFotoEmpleado($method, $requestUrl, $file, $fileName, $idEmpleado){
     
        $client=new Client([
            'base_uri'=>$this->baseUri,
        ]);
     
        if(isset($this->keyAccess)){
            $headers['Autorization']=$this->keyAccess;
        }
        $response = $client->request($method, $requestUrl, 
        
        [
            "headers" => [
                "Autorization" => $this->keyAccess,
               
                 "enctype" => "multipart/form-data"
            ],
            "multipart" => [
                [
                    "name" => "file",
                    "contents" => file_get_contents($file),
                    "filename" => $fileName
                ],
                [
                    "name" => "idEmpleado",
                    "contents" => $idEmpleado,
                ]
            ]
        ]
        );
 
   
        return $response->getBody()->getContents();
    } */
}