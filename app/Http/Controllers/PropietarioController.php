<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Traits\ApiResponser;
use Illuminate\Support\Collection;
use App\Services\Vehiculos\VehiculosService;
class PropietarioController extends Controller
{
    use ApiResponser;
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    protected $vehiculosService;
    public function __construct(VehiculosService $vehiculosService)
    {
        $this->middleware('auth');
        $this->vehiculosService=$vehiculosService;
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function propietarios()
    {
        $propietarios=$this->successResponse($this->vehiculosService->listPropietarios());
        $array = json_decode($propietarios->content());
        $propietarios=Collection::make($array->data);
        return view('layouts/propietarios/verPropietarios')->with(compact('propietarios'));
    }

    public function regNewPropietario(Request $request){
        $regPropietario=$this->successResponse($this->vehiculosService->regNewPropietario($request->all()));
        return $regPropietario;
    }

    public function consultaPropietarioID(){
        $numID=$_GET['numIDPropietario'];
        $propietario=$this->successResponse($this->vehiculosService->infoPropietarioID($numID));
        return $propietario;
    }
}