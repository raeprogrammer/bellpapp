<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Traits\ApiResponser;
use Illuminate\Support\Collection;
use App\Services\Vehiculos\VehiculosService;
class VehiculoController extends Controller
{
    use ApiResponser;
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    protected $vehiculosService;
    public function __construct(VehiculosService $vehiculosService)
    {
        $this->middleware('auth');
        $this->vehiculosService=$vehiculosService;
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function vehiculos()
    {
        $vehiculos=$this->successResponse($this->vehiculosService->listVehiculos());
        $array = json_decode($vehiculos->content());
        $vehiculos=Collection::make($array->data); 

        return view('layouts/vehiculos/verVehiculos')->with(compact('vehiculos'));
    }

    public function regNewVehiculo(Request $request){
        $regVehiculo=$this->successResponse($this->vehiculosService->regNewVehiculo($request->all()));
        return $regVehiculo;
    }

    public function infoVehiculoPlaca(){
        $placaVeh=$_GET['placa'];
        $tipoVeh=$_GET['tipoVeh'];

        $infoVehiculo=$this->successResponse($this->vehiculosService->infoVehiculoPlaca($placaVeh, $tipoVeh));
        return $infoVehiculo;
    }
}
