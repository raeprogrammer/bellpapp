<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Traits\ApiResponser;
use Illuminate\Support\Collection;
use App\Services\Parqueo\ParqueoService;
use App\Services\Vehiculos\VehiculosService;
class ParqueoController extends Controller
{
    use ApiResponser;
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    protected $parqueoService;
    public function __construct(ParqueoService $parqueoService)
    {
        $this->middleware('auth');
        $this->parqueoService=$parqueoService;
    }

    public function parqueo(){
        $vehiculosService=new VehiculosService;
        $posiciones=$this->successResponse($this->parqueoService->listPosiciones());
        $array = json_decode($posiciones->content());
        $posiciones=Collection::make($array->data);
       // dd($posiciones);
        /* Se obtienen posiciones ocupadas */
        $posicionesOcupadas=$posiciones->where('estado', '=', "Ocupado");
        foreach ($posicionesOcupadas as $posicionOcupada) {
            # Se consulta info Vehiculo
            $infoVehiculo=$this->successResponse($vehiculosService->infoVehiculoPlaca($posicionOcupada->placaVehiculo, $posicionOcupada->tipoVehiculoPosicion));
            $array = json_decode($infoVehiculo->content());
            $infoVehiculo=Collection::make($array->data);

            $posicionOcupada->marcaVeh=$infoVehiculo[0]->marca;
            $posicionOcupada->modeloVeh=$infoVehiculo[0]->modelo;
            
        }
        $posicionesAutos=$posiciones->where('tipoVehiculoPosicion', '=', "Automovil")->sortBy('numPosicion');
        $posicionesMoto=$posiciones->where('tipoVehiculoPosicion', '=', "Moto");
        $posicionesBicicleta=$posiciones->where('tipoVehiculoPosicion', '=', "Bicicleta");
       
        return view('layouts/parqueo/verParqueo')->with(compact('posicionesAutos', 'posicionesMoto', 'posicionesBicicleta'));
    }

    public function posicionesDisponibles(){
        $tipoVeh=$_GET['tipoVehiculo'];
        $posicionesDisponibles=$this->successResponse($this->parqueoService->listPosicionesLibres($tipoVeh));
        return $posicionesDisponibles;
    }

    public function regNewParqueo(Request $request){
        $regParqueo=$this->successResponse($this->parqueoService->regNewParqueo($request->all()));
        return $regParqueo;
    }

    public function retirarVehiculo(){
        $idPosicion=$_GET['idPosicion'];
        $retirarVeh=$this->successResponse($this->parqueoService->retirarVehiculo($idPosicion));
        return $retirarVeh;
    }
}