<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Traits\ApiResponser;
use Illuminate\Support\Collection;
use App\Services\Tarifas\TarifasService;
class TarifaController extends Controller
{
    use ApiResponser;
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    protected $tarifasService;
    public function __construct(TarifasService $tarifasService)
    {
        $this->middleware('auth');
        $this->tarifasService=$tarifasService;
    }

    public function tarifas(){
        $tarifas=$this->successResponse($this->tarifasService->listTarifas());
        $array = json_decode($tarifas->content());
        $tarifas=Collection::make($array->data); 

        $promocion=$this->successResponse($this->tarifasService->listPromocion());
        $array = json_decode($promocion->content());
        $promocion=Collection::make($array->data); 

        return view('layouts/tarifas/verTarifas')->with(compact('tarifas', 'promocion'));
    }

    public function regNewTarifa(Request $request){
        $regTarifa=$this->successResponse($this->tarifasService->regNewTarifa($request->all()));
        return $regTarifa;
    }

    public function regPromocion(Request $request){
        $regPromocion=$this->successResponse($this->tarifasService->regNewPromocion($request->all()));
        return $regPromocion;
    }
}